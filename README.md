# Kaypay SDK for PHP

## Installation

The recommended way to install Kaypay SDK for PHP is through Composer:

```sh
composer require kaypay/sdk
```

## Usage

### Setup API instance

```php
// obtain the credential from Kaypay Account Manager
$merchantCode = 'AWESOME_MERCHANT';
$secretKey = 's3cret';

// call the constructor
$signer = new \Kaypay\Sdk\Signer($secretKey);
$api = new \Kaypay\Sdk\Api\OrderApi($signer);
```

For sandbox environment:

```php
$sandbox = new \Kaypay\Sdk\SandboxConfiguration();
$api = new \Kaypay\Sdk\Api\OrderApi($signer, null, $sandbox);
```

### Create Kaypay order

```php
// prepare the request body with order details
// `$merchantRefId` will be used in webhook callbacks to identify orders
$requestBody = (new \Kaypay\Sdk\Model\OrderCreateRequestBody())
    ->setMerchantCode($merchantCode)
    ->setMerchantRefId($merchantRefId)
    ->setTotalAmount(1000000);

// send the API request
$result = $this->api->postV1Orders($requestBody);

// redirect user to start the payment flow
$redirectUrl = $result->getData()->getRedirectUrl();
```
