<?php

namespace Kaypay\Sdk\Test;

use Kaypay\Sdk\Signer;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Kaypay\Sdk\Signer
 */
class SignerTest extends TestCase
{
    const DATA = '{"foo":"bar"}';

    const SECRET_KEY = 's3cret';

    /**
     * @var Signer
     */
    private $signer;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->signer = new Signer(self::SECRET_KEY);
    }

    public function testGetVersion()
    {
        $version = $this->signer->getVersion();
        $this->assertEquals('v1', $version);
    }

    public function testMakeHeaders()
    {
        $headers = $this->signer->makeHeaders(self::DATA);
        $this->assertEquals([
            'X-Kaypay-Signature' => 'v1=/fcKOYJllgEc0g5zGa5tolBHTXGq7L3j5LmbalcGV1c=',
        ], $headers);
    }

    public function testSignData()
    {
        $signature = $this->signer->signData(self::DATA);
        $this->assertEquals('v1=/fcKOYJllgEc0g5zGa5tolBHTXGq7L3j5LmbalcGV1c=', $signature);
    }

    public function testVerify()
    {
        $verified = $this->signer->verifySignature(self::DATA, 'v1=/fcKOYJllgEc0g5zGa5tolBHTXGq7L3j5LmbalcGV1c=');
        $this->assertTrue($verified);
    }

    public function testVerifyFalse()
    {
        $verified = $this->signer->verifySignature(self::DATA, 'foo');
        $this->assertFalse($verified);
    }
}
