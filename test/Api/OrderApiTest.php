<?php

namespace Kaypay\Sdk\Test\Api;

use GuzzleHttp\Exception\GuzzleException;
use Kaypay\Sdk\Api\OrderApi;
use Kaypay\Sdk\ApiException;
use Kaypay\Sdk\Model\Order;
use Kaypay\Sdk\Model\OrderItem;
use Kaypay\Sdk\Model\MetadataCreateRequestBody;
use Kaypay\Sdk\Model\MetadataFulfillment;
use Kaypay\Sdk\Model\OrderCreateRequestBody;
use Kaypay\Sdk\SandboxConfiguration;
use Kaypay\Sdk\Signer;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Kaypay\Sdk\Api\OrderApi
 */
class OrderApiTest extends TestCase
{
    /** @var OrderApi */
    protected $api;

    protected function setUp(): void
    {
        $sandbox = new SandboxConfiguration();
        $signer = new Signer('s3cret');
        $this->api = new OrderApi($signer, null, $sandbox);
    }

    /**
     * @throws ApiException
     * @throws GuzzleException
     */
    public function testSync()
    {
        $merchantRefId = sprintf('%s-%d', $this->getName(), time());
        $postRequestBody = $this->buildOrderCreateRequestBody($merchantRefId);

        // postV1Orders
        $postResult = $this->api->postV1Orders($postRequestBody);
        $this->assertInstanceOf('\Kaypay\Sdk\Model\OrderCreateSuccess', $postResult);
        $postData = $postResult->getData();
        $this->assertInstanceOf('\Kaypay\Sdk\Model\OrderCreateSuccessData', $postData);
        $orderId = $postData->getOrderId();
        $this->assertNotEmpty($orderId);

        // getV1OrdersOrderId
        $getResult = $this->api->getV1OrdersOrderId($orderId);
        $this->assertInstanceOf('\Kaypay\Sdk\Model\OrderGetSuccess', $getResult);
        $getOrder = $getResult->getData();
        $this->assertInstanceOf('\Kaypay\Sdk\Model\Order', $getOrder);
        $this->assertEquals($orderId, $getOrder->getId());
        $this->assertOrderProperties($merchantRefId, $getOrder);

        // putV1Orders
        $putRequestBody = $this->buildMetadataCreateRequestBody($merchantRefId);
        $putResult = $this->api->putV1Orders($putRequestBody);
        $this->assertNotEmpty($putResult);
    }

    public function testAsync()
    {
        $merchantRefId = sprintf('%s-%d', $this->getName(), time());
        $postRequestBody = $this->buildOrderCreateRequestBody($merchantRefId);

        // postV1OrdersAsync
        $postResult = $this->api->postV1OrdersAsync($postRequestBody)->wait();
        $this->assertInstanceOf('\Kaypay\Sdk\Model\OrderCreateSuccess', $postResult);
        $postData = $postResult->getData();
        $this->assertInstanceOf('\Kaypay\Sdk\Model\OrderCreateSuccessData', $postData);
        $orderId = $postData->getOrderId();
        $this->assertNotEmpty($orderId);

        // getV1OrdersOrderIdAsync
        $getResult = $this->api->getV1OrdersOrderIdAsync($orderId)->wait();
        $this->assertInstanceOf('\Kaypay\Sdk\Model\OrderGetSuccess', $getResult);
        $getOrder = $getResult->getData();
        $this->assertInstanceOf('\Kaypay\Sdk\Model\Order', $getOrder);
        $this->assertEquals($orderId, $getOrder->getId());
        $this->assertOrderProperties($merchantRefId, $getOrder);

        // putV1OrdersAsync
        $putRequestBody = $this->buildMetadataCreateRequestBody($merchantRefId);
        $putResult = $this->api->putV1OrdersAsync($putRequestBody)->wait();
        $this->assertNotEmpty($putResult);
    }

    /**
     * @param string $merchantRefId
     * @param Order $order
     * @return void
     */
    protected function assertOrderProperties($merchantRefId, $order)
    {
        $this->assertEquals('https://api.merchant.com/webhooks/order', $order->getCallbackUrl());
        $this->assertEquals('VND', $order->getCurrency());
        $this->assertEquals('BNPL for order #ORDER123 at Awesome Merchant', $order->getDescription());
        $this->assertEquals('AWESOME_MERCHANT', $order->getMerchantCode());
        $this->assertEquals($merchantRefId, $order->getMerchantRefId());
        $this->assertEquals('https://merchant.com/checkout/thank-you', $order->getReturnUrl());
        $this->assertEquals(25000, $order->getShippingFee());
        $this->assertEquals(1000000, $order->getTotalAmount());

        $orderItems = $order->getOrderItems();
        $this->assertCount(1, $orderItems);

        $orderItem = $orderItems[0];
        $this->assertEquals(10000, $orderItem->getPrice());
        $this->assertEquals('https://merchant.com/images/paper.png', $orderItem->getProductImage());
        $this->assertEquals('Paper', $orderItem->getProductName());
        $this->assertEquals(100, $orderItem->getQuantity());
        $this->assertEquals('RED_PAPER', $orderItem->getSku());
        $this->assertEquals('https://merchant.com/images/paper/red.png', $orderItem->getSkuImage());
        $this->assertEquals('Red paper', $orderItem->getSkuName());
    }

    /**
     * @return OrderCreateRequestBody
     */
    protected function buildOrderCreateRequestBody($merchantRefId)
    {
        /** @var OrderItem[] $orderItems */
        $orderItems = [];
        $orderItems[] = (new OrderItem())
            ->setPrice(10000)
            ->setProductImage('https://merchant.com/images/paper.png')
            ->setProductName('Paper')
            ->setQuantity(100)
            ->setSku('RED_PAPER')
            ->setSkuImage('https://merchant.com/images/paper/red.png')
            ->setSkuName('Red paper');

        return (new OrderCreateRequestBody())
            ->setCallbackUrl('https://api.merchant.com/webhooks/order')
            ->setCurrency('VND')
            ->setDescription('BNPL for order #ORDER123 at Awesome Merchant')
            ->setMerchantCode('AWESOME_MERCHANT')
            ->setMerchantRefId($merchantRefId)
            ->setOrderItems($orderItems)
            ->setReturnUrl('https://merchant.com/checkout/thank-you')
            ->setShippingFee(25000)
            ->setTotalAmount(1000000);
    }

    protected function buildMetadataCreateRequestBody($merchantRefId)
    {
        /** @var MetadataFulfillment[] $fulfillments */
        $fulfillments = [];
        $fulfillments[] = (new MetadataFulfillment())
            ->setShipmentStatus(MetadataFulfillment::SHIPMENT_STATUS_DELIVERED)
            ->setTrackingId('TRACK123')
            ->setTrackingUrl('https://shipper.com/trackings/123');

        return (new MetadataCreateRequestBody())
            ->setFulfillments($fulfillments)
            ->setMerchantCode('AWESOME_MERCHANT')
            ->setMerchantRefId($merchantRefId);
    }
}
