#!/bin/bash

set -e

cd "$(dirname "${BASH_SOURCE[0]}")/.."

if ! which openapi-generator >/dev/null 2>&1; then
  # install with Homebrew if our generator is not available
  brew install openapi-generator
fi

_tmpPath=./openapi.json
curl https://payment-api.kaypay.io/swagger/openapi.json |
  jq 'del(.paths."/v1/orders/{orderId}".put)' |
  jq '.servers[0].description = "Kaypay Payment Gateway - Production"' |
  jq '.servers[0].url = "https://payment-api.kaypay.net"' |
  tee "${_tmpPath}"

rm -rf lib

openapi-generator generate \
  -i "${_tmpPath}" -g php -o . \
  --additional-properties=invokerPackage=Kaypay\\Sdk,packageName=kaypay/sdk,variableNamingConvention=camelCase

rm -rf docs
rm -rf test

./vendor/bin/php-cs-fixer fix --allow-risky=yes
