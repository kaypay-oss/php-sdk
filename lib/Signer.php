<?php

namespace Kaypay\Sdk;

class Signer
{
    public const SIGNATURE_HEADER = 'X-Kaypay-Signature';

    public const SIGNATURE_VERSION1 = 'v1';

    /**
     * @var string
     */
    private $secretKey;

    /**
     * @var string
     */
    private $version;

    /**
     * @param string $secretKey
     * @param string $version
     */
    public function __construct($secretKey, $version = self::SIGNATURE_VERSION1)
    {
        $this->secretKey = $secretKey;
        $this->version = $version;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param string $data
     * @return string[]
     */
    public function makeHeaders($data)
    {
        return [
            self::SIGNATURE_HEADER => $this->signData($data),
        ];
    }

    /**
     * @param string $data
     * @return string
     */
    public function signData($data)
    {
        // v1 algorithm
        $hashed = hash_hmac('sha256', $data, $this->secretKey, true);
        return sprintf('%s=%s', $this->version, base64_encode($hashed));
    }

    /**
     * @param string $data
     * @param string $signature
     * @return bool
     */
    public function verifySignature($data, $signature)
    {
        $expected = $this->signData($data);
        $actual = explode(',', $signature);
        return in_array($expected, $actual, true);
    }
}
