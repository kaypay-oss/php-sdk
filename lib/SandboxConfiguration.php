<?php

namespace Kaypay\Sdk;

class SandboxConfiguration extends Configuration
{
    public function __construct()
    {
        parent::__construct();
        $this->setHost('https://payment-api.sandbox.kaypay.io');
    }
}
